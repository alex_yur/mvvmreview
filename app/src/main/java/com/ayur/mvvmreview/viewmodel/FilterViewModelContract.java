package com.ayur.mvvmreview.viewmodel;

import android.app.Activity;

import com.ayur.mvvmreview.model.JobType;
import com.ayur.mvvmreview.model.Tag;

import java.util.List;

/**
 * Created by oleksiyyur on 10/20/16.
 */

public interface FilterViewModelContract {

    interface FilterView{
        void setJobTypesList(List<JobType> jobTypes);
        void setJobIndustriesList(List<Tag> industriesList);
        void showSuggestions(List<String> suggestions);
        Activity getActivity();
        List<String> getSelectedIndustries();
        List<String> getSelectedJobTypes();
    }

}
