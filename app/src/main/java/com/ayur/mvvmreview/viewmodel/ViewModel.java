package com.ayur.mvvmreview.viewmodel;

/**
 * Created by oleksiyyur on 10/20/16.
 */

public interface ViewModel {
    void destroy();
    void init();
}
