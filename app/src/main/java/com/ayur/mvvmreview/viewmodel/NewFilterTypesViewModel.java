package com.ayur.mvvmreview.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.ayur.mvvmreview.model.JobType;


public class NewFilterTypesViewModel extends BaseObservable {

    private JobType jobType;
    public ObservableBoolean isSelected;

    public NewFilterTypesViewModel(JobType jobType) {
        this.jobType = jobType;
        isSelected = new ObservableBoolean(jobType.isSelected());
    }

    public String getTypeName(){
        return jobType.getName();
    }

    public void onItemClick(View view){
        boolean isSelectedInner = !isSelected.get();
        isSelected.set(isSelectedInner);
        jobType.setSelected(isSelectedInner);
    }

    public void setJobType(JobType jobType){
        this.jobType = jobType;
        isSelected.set(jobType.isSelected());
        notifyChange();
    }
}
