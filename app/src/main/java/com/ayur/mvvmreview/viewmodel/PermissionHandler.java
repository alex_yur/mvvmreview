package com.ayur.mvvmreview.viewmodel;

import android.support.annotation.NonNull;

/**
 * Created by oleksiyyur on 10/25/16.
 */

public interface PermissionHandler {
    void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);
}
