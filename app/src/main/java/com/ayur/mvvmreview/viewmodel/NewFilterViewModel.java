package com.ayur.mvvmreview.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.ObservableInt;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ayur.mvvmreview.R;
import com.ayur.mvvmreview.gps.MyLocationListener;
import com.ayur.mvvmreview.model.JobType;
import com.ayur.mvvmreview.model.Tag;
import com.ayur.mvvmreview.utils.LocationUtils;
import com.ayur.mvvmreview.utils.RxUtils;
import com.ayur.mvvmreview.views.ObservableString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import rx.Observable;
import rx.Subscription;

import static android.content.Context.LOCATION_SERVICE;


public class NewFilterViewModel implements ViewModel, PermissionHandler {
    public static final String TAG = "New Filter View Model";

    public ObservableString searchQueryField;
    public ObservableString searchLocationField;
    public ObservableInt suggestionsScreenVisibility;

    private Context context;
    private FilterViewModelContract.FilterView filterView;

    private Observable<String> locationFieldObservable;

    private Subscription locationSubscription;
    private Subscription tagsSubscription;
    private List<Tag> mTags;

   /* @BindDrawable(R.drawable.delete_text_violet_icon) Drawable mDelete;
    @BindDrawable(R.drawable.location_violet_icon) Drawable mLocationIcon;
    @BindDrawable(R.drawable.search_violet_icon) Drawable mSearchIcon;*/

    Drawable mDelete;
    Drawable mLocationIcon;
    Drawable mSearchIcon;

    public NewFilterViewModel(Context context, FilterViewModelContract.FilterView filterView) {
        //ButterKnife.bind(this, filterView.getActivity());

        this.context = context;
        this.filterView = filterView;

        this.searchQueryField = new ObservableString("");
        this.searchLocationField = new ObservableString("");

        this.suggestionsScreenVisibility = new ObservableInt(View.GONE);
        this.locationFieldObservable = RxUtils.toObservable(searchLocationField);

        initSubscriptions();
    }

    @Override
    public void init() {
        loadTypes();
        loadIndustries();
    }

    public void clearFilter(View view){
        init();
    }

    public void applyFilter(View view){
        //FilterData fd = getFilterData();

        if(true){
            //Intent intent = new Intent(filterView.getActivity(), JobSearchActivity.class);
            //intent.putExtra(JobSearchActivity.FILTER_DATA, fd);
            //filterView.getActivity().startActivity(intent);
            filterView.getActivity().overridePendingTransition(0, 0);
        } else {
            Toast.makeText(filterView.getActivity(), "You should choose some filters to apply", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadTypes(){
        filterView.setJobTypesList(new ArrayList<JobType>()/*JobTypes.getJobTypesList()*/);
    }

    public View.OnFocusChangeListener onFocusChanged(){
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                boolean isLocation = v.getId() == R.id.search_location;

                if(isLocation){
                    NewFilterViewModel.this.suggestionsScreenVisibility.set(hasFocus ? View.VISIBLE : View.GONE);
                }

                ((EditText) v).setCompoundDrawablesWithIntrinsicBounds(isLocation ? mLocationIcon : mSearchIcon, null, hasFocus ? mDelete : null, null);
            }
        };
    };

    public View.OnTouchListener onTouch(){
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                EditText editText = (EditText) v;
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(editText.getCompoundDrawables()[DRAWABLE_RIGHT] == null){
                        return false;
                    }

                    if(event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if(editText.getText().toString().equals("")){
                            //Utils.hideKeyboard(filterView.getActivity());
                        }
                        editText.setText("");

                        return true;
                    }
                }
                return false;
            }
        };
    }

    public TextView.OnEditorActionListener onEditorAction(){
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                v.clearFocus();
                //Utils.hideKeyboard(filterView.getActivity());
                return true;
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == LocationUtils.LOCATION_PERMISSION_REQUEST_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                requestLocation();
            }
        }
    }

    private void requestLocation(){
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        LocationListener locationListener = new MyLocationListener(locationManager, filterView.getActivity(), new MyLocationListener.OnLocationRedy() {
            @Override
            public void onLocation(Location location) {
                handleNewLocation(location);
            }
        });

        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            try{
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            } catch (SecurityException se){
                Log.d(TAG, "Security Exception", se);
            }
        } else {
            //new NoGPSWarningDialog(filterView.getActivity()).show();
        }
    }

    private void handleNewLocation(Location location){
        Geocoder geocoder = new Geocoder(context, Locale.GERMAN);
        try {
            android.location.Address address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0);
            System.out.println(address);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void requestCurrentLocation(View view){
        /*if(LocationUtils.isPermissionGranted(context)){
            requestLocation();
        }else{
            LocationUtils.requestPermission(filterView.getActivity());
        }*/
    }

    private void /*FilterData*/ getFilterData(){
        /*FilterData filterData = new FilterData();
        filterData.setLocation(searchLocationField.get());
        filterData.setSearchQuery(searchQueryField.get());
        filterData.setIndustriesList(filterView.getSelectedIndustries());
        filterData.setJobTypesList(filterView.getSelectedJobTypes());

        return filterData;
        */
    }

    private void loadIndustries(){
        if(mTags != null){
            filterView.setJobIndustriesList(mTags);
            return;
        }

        /**
            mTags = tags;
            filterView.setJobIndustriesList(mTags);
         */

       /* tagsSubscription = RestClient.getApiService().getTags()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Tag>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Tag> tags) {
                        mTags = tags;
                        filterView.setJobIndustriesList(mTags);
                    }
                });*/
    }

    public void fillChosenSuggestion(String suggestion) {
        unSubscribe();
        this.searchLocationField.set(suggestion);
        this.suggestionsScreenVisibility.set(View.GONE);
        //Utils.hideKeyboard(filterView.getActivity());
        initSubscriptions();
    }

    /**
     * Loads and delegates suggested cities to view.
     */
    private void initSubscriptions() {
        /**
            suggestionsScreenVisibility.set(View.VISIBLE);
            filterView.showSuggestions(KTUtils.getNames(cities));
         */

        /*locationSubscription = locationFieldObservable
                .debounce(400, TimeUnit.MILLISECONDS)
                .switchMap(string -> RestClient.getApiService().getCityList(string))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<City>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Error in cities request", e);
                    }

                    @Override
                    public void onNext(List<City> cities) {
                        suggestionsScreenVisibility.set(View.VISIBLE);
                        filterView.showSuggestions(KTUtils.getNames(cities));
                    }
                });*/
    }

    private void unSubscribe(){
        if(this.locationSubscription != null && !this.locationSubscription.isUnsubscribed())
            this.locationSubscription.unsubscribe();

        if(this.tagsSubscription != null && !this.tagsSubscription.isUnsubscribed())
            this.tagsSubscription.unsubscribe();
    }

    @Override
    public void destroy() {
        this.context = null;
        this.filterView = null;
        unSubscribe();
    }
}
