package com.ayur.mvvmreview.activities;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.ayur.mvvmreview.R;
import com.ayur.mvvmreview.adapter.FilterTypesAdapter;
import com.ayur.mvvmreview.adapter.SuggestionsAdapter;
import com.ayur.mvvmreview.databinding.NewFilterActivityBinding;
import com.ayur.mvvmreview.model.JobType;
import com.ayur.mvvmreview.model.Tag;
import com.ayur.mvvmreview.utils.ViewUtils;
import com.ayur.mvvmreview.viewmodel.FilterViewModelContract;
import com.ayur.mvvmreview.viewmodel.NewFilterViewModel;
import com.google.android.flexbox.FlexboxLayout;

import java.util.LinkedList;
import java.util.List;



public class NewFilterActivity extends AppCompatActivity implements FilterViewModelContract.FilterView {

    private NewFilterActivityBinding newFilterActivityBinding;
    private NewFilterViewModel newFilterViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        newFilterActivityBinding = DataBindingUtil.setContentView(this, R.layout.new_filter_activity);
        newFilterViewModel = new NewFilterViewModel(this, this);
        newFilterActivityBinding.setMainViewModel(newFilterViewModel);

        newFilterActivityBinding.additionalFiltersHolder.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                newFilterActivityBinding.additionalFiltersHolder.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                newFilterActivityBinding.setView(NewFilterActivity.this);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        newFilterViewModel.init();
    }

    /**
     * Expands and collapses filter.
     */
    public View.OnClickListener expandCollapse(){
        final int buttonHeight = newFilterActivityBinding.filterButton.getHeight();
        final int minimalFilDist = 50;
        final ViewGroup filterHolder = newFilterActivityBinding.additionalFiltersHolder;

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filterHolder.getHeight() <= buttonHeight + minimalFilDist){
                    //Expand filter
                    ///Utils.expand1(filterHolder, buttonHeight);
                } else {
                    //COllapse filter
                    //Utils.collapse1(filterHolder, buttonHeight);
                }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        newFilterViewModel.destroy();
    }

    private void initTypesList(){
        if(newFilterActivityBinding.jobTypes.getAdapter() != null)
            return;

        RecyclerView recyclerView = newFilterActivityBinding.jobTypes;
        FilterTypesAdapter filterTypesAdapter = new FilterTypesAdapter();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setAdapter(filterTypesAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void setJobTypesList(List<JobType> jobTypes) {
        if(this.newFilterActivityBinding.jobTypes.getAdapter() == null)
            initTypesList();

        FilterTypesAdapter fa = (FilterTypesAdapter)this.newFilterActivityBinding.jobTypes.getAdapter();
        fa.setTypesList(jobTypes);
    }

    /**
     * Fills flexbox layout with industries views,
     * @param industriesList
     */
    @Override
    public void setJobIndustriesList(List<Tag> industriesList) {
        FlexboxLayout pl = newFilterActivityBinding.industriesHolder;
        pl.removeAllViews();
        for(View v : ViewUtils.getIndustriesViews(industriesList, this)){
            pl.addView(v);
        }
    }


    /**
     * returns selected job types.
     * @return list with selected job types names.
     */
    @Override
    public List<String> getSelectedJobTypes(){
        RecyclerView recyclerView = newFilterActivityBinding.jobTypes;
        FilterTypesAdapter fa = (FilterTypesAdapter)recyclerView.getAdapter();
        return new LinkedList<>(); //KTUtils.getSelectedJobTypes(fa.getJobTypes());
    }

    /**
     * returns selected industries.
     * @return list with selected industries names.
     */
    @Override
    public List<String> getSelectedIndustries(){
        ViewGroup viewGroup = newFilterActivityBinding.industriesHolder;
        return new LinkedList<>(); //KTUtils.getSelectedViews(viewGroup);
    }

    private void initSuggestions(List<String> suggestions){
        LinearLayoutManager llm = new LinearLayoutManager(this);
        SuggestionsAdapter suggestionsAdapter = new SuggestionsAdapter(suggestions, newFilterViewModel);
        newFilterActivityBinding.suggestionsList.setLayoutManager(llm);
        newFilterActivityBinding.suggestionsList.setAdapter(suggestionsAdapter);
    }

    @Override
    public void showSuggestions(List<String> suggestions) {
        SuggestionsAdapter adapter = (SuggestionsAdapter)newFilterActivityBinding.suggestionsList.getAdapter();

        if(adapter == null){
            initSuggestions(suggestions);
        } else {
            adapter.setNewSuggestions(suggestions);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        newFilterViewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public void back(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
    }
}