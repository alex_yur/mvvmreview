package com.ayur.mvvmreview.utils;

import android.databinding.BindingAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;

import com.ayur.mvvmreview.R;
import com.ayur.mvvmreview.views.ObservableString;


/**
 * Created by oleksiyyur on 10/22/16.
 */

public class BindingUtils {

    @BindingAdapter({"bind:isSelected"})
    public static void isSelected(View view, boolean isSelected){
        view.setSelected(isSelected);
    }

    @BindingAdapter("android:text")
    public static void bindEditText(EditText view, final ObservableString observableString) {
        Pair<ObservableString, TextWatcher> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != observableString) {
            if (pair != null) view.removeTextChangedListener(pair.second);
            TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    observableString.set(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(observableString, watcher));
            view.addTextChangedListener(watcher);
        }
        String newValue = observableString.get();
        if (!view.getText().toString().equals(newValue))
            view.setText(newValue);
    }

}
