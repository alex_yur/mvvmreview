package com.ayur.mvvmreview.utils;


import com.ayur.mvvmreview.App;

/**
 * Created by oleksiyyur on 10/21/16.
 */

public class ResUtils {
    public static int getColor(int id){
        return App.getContext().getResources().getColor(id);
    }
}
