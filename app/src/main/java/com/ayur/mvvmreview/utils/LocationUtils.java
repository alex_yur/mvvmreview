package com.ayur.mvvmreview.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

public class LocationUtils {
    public static int LOCATION_PERMISSION_REQUEST_CODE = 666;
    public static final String[] LOCATION_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    public static boolean isPermissionGranted(Context context){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            int permissionStatus = context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if(permissionStatus != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }

        return true;
    }

    public static void requestPermission(Activity activity){
        ActivityCompat.requestPermissions(
                activity,
                LOCATION_PERMISSIONS,
                LOCATION_PERMISSION_REQUEST_CODE);
    }
}
