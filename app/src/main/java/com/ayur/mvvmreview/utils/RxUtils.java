package com.ayur.mvvmreview.utils;

import android.support.annotation.NonNull;


import com.ayur.mvvmreview.views.ObservableString;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

public class RxUtils {
    public static <T> Observable<String> toObservable(@NonNull final ObservableString field) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(final Subscriber<? super String> subscriber) {
                final android.databinding.Observable.OnPropertyChangedCallback callback = new android.databinding.Observable.OnPropertyChangedCallback() {
                    @Override
                    public void onPropertyChanged(android.databinding.Observable observable, int i) {
                        subscriber.onNext(field.get());
                    }
                };
                field.addOnPropertyChangedCallback(callback);
                subscriber.add(Subscriptions.create(new Action0() {
                    @Override
                    public void call() {
                        field.removeOnPropertyChangedCallback(callback);
                    }
                }));
            }
        });
    }
}
