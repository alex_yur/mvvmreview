package com.ayur.mvvmreview.utils;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.ayur.mvvmreview.model.Tag;

import java.util.LinkedList;
import java.util.List;


public class ViewUtils {

    public static List<View> getIndustriesViews(List<Tag> industries, Context context){
        List<View> result = new LinkedList<>();

        for(Tag tag : industries){
            result.add(getSingleView(tag.getName(), context));
        }

        return result;
    }

    private static TextView getSingleView(String text, Context context){
        final TextView textView = new TextView(context);
        final int mWhite = ResUtils.getColor(android.R.color.white);
        final int mVioletText = ResUtils.getColor(android.R.color.holo_red_dark);

        textView.setText(text);
        //textView.setBackgroundResource(R.drawable.job_industry_bg);
        textView.setTextColor(mVioletText);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = !v.isSelected();
                v.setSelected(isSelected);
                textView.setTextColor(isSelected ? mWhite : mVioletText);
            }
        });

        return textView;
    }
}
