package com.ayur.mvvmreview;

import android.app.Application;
import android.content.Context;

/**
 * Created by oleksiyyur on 10/25/16.
 */

public class App extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext(){
        return context;
    }
}
