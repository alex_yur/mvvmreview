package com.ayur.mvvmreview.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ayur.mvvmreview.viewmodel.NewFilterViewModel;

import java.util.List;

public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.SuggestionsHolder>{
    private List<String> suggestions;
    private NewFilterViewModel filterView;

    public SuggestionsAdapter(List<String> suggestions, NewFilterViewModel filterView) {
        this.suggestions = suggestions;
        this.filterView = filterView;
    }

    @Override
    public SuggestionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SuggestionsHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_suggestions_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(SuggestionsHolder holder, int position) {
        holder.mText.setText(suggestions.get(position));
    }

    @Override
    public int getItemCount() {
        return suggestions.size();
    }

    public void setNewSuggestions(List<String> suggestions){
        this.suggestions = suggestions;
        notifyDataSetChanged();
    }

    public class SuggestionsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        //@BindView(R.id.text)
        TextView mText;

        public SuggestionsHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            //ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            filterView.fillChosenSuggestion(mText.getText().toString());
        }
    }
}