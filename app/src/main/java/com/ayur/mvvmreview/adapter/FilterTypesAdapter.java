package com.ayur.mvvmreview.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ayur.mvvmreview.databinding.NewFilterTypesViewBinding;
import com.ayur.mvvmreview.model.JobType;
import com.ayur.mvvmreview.viewmodel.NewFilterTypesViewModel;

import java.util.List;


public class FilterTypesAdapter extends RecyclerView.Adapter<FilterTypesAdapter.TypesHolder> {

    private List<JobType> jobTypes;

    public FilterTypesAdapter() {
    }

    public FilterTypesAdapter(List<JobType> jobTypes) {
        this.jobTypes = jobTypes;
    }

    @Override
    public TypesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NewFilterTypesViewBinding newFilterTypesViewBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.new_filter_types_view,
                        parent, false);
        return new TypesHolder(newFilterTypesViewBinding);
    }

    @Override
    public void onBindViewHolder(TypesHolder holder, int position) {
        holder.bindType(jobTypes.get(position));
    }

    public void setTypesList(List<JobType> jobTypes){
        this.jobTypes = jobTypes;
        notifyDataSetChanged();
    }

    @Override
    public void onViewRecycled(TypesHolder holder) {
        super.onViewRecycled(holder);
        holder.newFilterTypesViewBinding.item.setSelected(false);
    }

    public List<JobType> getJobTypes(){
        return jobTypes;
    }

    @Override
    public int getItemCount() {
        return jobTypes.size();
    }

    static class TypesHolder extends RecyclerView.ViewHolder{
        NewFilterTypesViewBinding newFilterTypesViewBinding;

        TypesHolder(NewFilterTypesViewBinding newFilterTypesViewBinding) {
            super(newFilterTypesViewBinding.item);
            this.newFilterTypesViewBinding = newFilterTypesViewBinding;
        }

        void bindType(JobType jobType){
            if(newFilterTypesViewBinding.getTypesViewModel() == null){
                newFilterTypesViewBinding.setTypesViewModel(new NewFilterTypesViewModel(jobType));
            } else {
                newFilterTypesViewBinding.getTypesViewModel().setJobType(jobType);
            }
        }
    }
}
