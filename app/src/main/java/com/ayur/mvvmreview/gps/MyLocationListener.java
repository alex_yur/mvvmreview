package com.ayur.mvvmreview.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

/**
 * Created by oleksiyyur on 10/24/16.
 */

public class MyLocationListener implements LocationListener {

    private LocationManager locationManager;
    private OnLocationRedy onLocationRedy;
    private Context context;

    public MyLocationListener(LocationManager locationManager, Context context, OnLocationRedy onLocationRedy) {
        this.locationManager = locationManager;
        this.onLocationRedy = onLocationRedy;
        this.context = context;
    }

    @Override
    public void onLocationChanged(Location location) {
        onLocationRedy.onLocation(location);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("OOps some premission s not granted");
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            context = null;
            return;
        }
        context = null;
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public interface OnLocationRedy{
        void onLocation(Location location);
    }
}