package com.ayur.mvvmreview.model;

/**
 * Created by oleksiyyur on 10/20/16.
 */

public class JobType {
    private String name;
    private String backendName;
    private boolean isSelected;

    public JobType(String name, String backendName) {
        this.name = name;
        this.backendName = backendName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackendName() {
        return backendName;
    }

    public void setBackendName(String backendName) {
        this.backendName = backendName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
