package com.ayur.mvvmreview.model;



public class Tag {

	String apiUrl;

	String name;

	String slug;

	int count;
	boolean isSelected;

	/**
	 *
	 * @return
	 * The apiUrl
	 */
	public String getApiUrl() {
		return apiUrl;
	}

	/**
	 *
	 * @param apiUrl
	 * The api_url
	 */
	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	/**
	 *
	 * @return
	 * The name
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 * The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 * The slug
	 */
	public String getSlug() {
		return slug;
	}

	/**
	 *
	 * @param slug
	 * The slug
	 */
	public void setSlug(String slug) {
		this.slug = slug;
	}

	/**
	 *
	 * @return
	 * The count
	 */
	public int getCount() {
		return count;
	}

	/**
	 *
	 * @param count
	 * The count
	 */
	public void setCount(int count) {
		this.count = count;
	}
	

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

}
